//Person module routes/person.js
const Person=require('./Person.js')
module.exports= [
    {
        //for creating new person
        method: 'POST',
        path: '/save',

        handler: async (request, h) => {
            try {
                var person = new Person(request.payload)
                var result = await person.save()
                return h.response(result)

            } catch (err) {
                
                return h.response(error).code(500)
            }

        }
    },

    {
        //fetching all available persons
        method: 'GET',
        path: '/fetch',
        handler: async (request, h) => {
            try {
                var person = await Person.find().exec()
                return h.response(person)
            }
            catch (error) {
                console.log('THe error is:'+error)
                return h.response(error).code(500)
            }

        }
    },
    {
        //for fetching info of perticular person by using id
        method: 'GET',
        path: '/fetch/{id}',
        handler: async (request, h) => {
            try {
                var person = await Person.findById(request.params.id).exec()
                return h.response(person)
            }
            catch (error) {
                return h.response(error).code(500)
            }

        }
    },
    {
        //for updating info of person by using id
        method: 'PUT',
        path: '/update/{id}',

        handler: async (request, h) => {
            try {

                var result = await Person.findByIdAndUpdate(request.params.id, request.payload, { new: true }).exec()
                return h.response(result)

            } catch (err) {
                return h.response(error).code(500)
            }

        }
    },
    {
        //for deleting perticular person by using id
        method: 'DELETE',
        path: '/delete/{id}',
        handler: async (request, h) => {
            try {
                var person = await Person.findByIdAndDelete(request.params.id).exec()
                return h.response(person)
            }
            catch (error) {
                return h.response(error).code(500)
            }

        }
    }

]
