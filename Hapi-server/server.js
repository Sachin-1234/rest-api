
const Hapi = require('@hapi/hapi')
const config = require('config')
//mongodb connection module
const mongocon = require('mongocon-abc12')
const Routes = require('./routes/route.js')

exports.init = () => {
    try {
        const server = new Hapi.Server({
            "host": config.server.host,
            "port": config.server.port
        })

        mongocon.init(config)

        for (const route in Routes) {
            server.route(Routes[route]);
        }

        server.start()

        server.events.on('response', request => {
            if (request.response) {
                console.log(`${request.info.remoteAddress}: ${request.method.toUpperCase()} ${request.url.pathname} --> ${request.response.statusCode}`);
            } else {
                console.log('No statusCode : ', request.info.remoteAddress + ': ' + request.method.toUpperCase() + ' ' + request.url.pathname + ' --> ');
            }
        });

        server.events.on('route', (route) => {

            console.log(`New route added: ${route.path}`);
        });
        server.events.on('start', (route) => {
            console.log('Node server is running on:', server.info.uri);
        });
        server.events.on('stop', (route) => {

            console.log('Server has been stopped');
        });
        return server;
    } catch (err) {
        console.log("Error starting server: ", err);
        throw err;
    }

}